var ip = require('ip');
var express = require('express');
var router = express.Router();
var myip = ip.address();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { ip: 'Someone is yelling on ' + myip });
});

module.exports = router;

console.log('Test me at http://localhost:3000');

console.log('...oh and... Someone is yelling on ' + myip);
